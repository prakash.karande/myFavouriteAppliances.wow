#My Favourite Appliances

An example Laravel and Vue.js App to allow simple Wishlist functionality.

## Tech Stack

* Laravel 5.6 (with Passport)
* Vue.Js 2.5 
* Bootstrap 4

## Minimum Requirements

* PHP 7.1+
* MySQL 5.7+ (Json data type used) [PgSQL 9+ should also work, but I did not test with Postgres]
* Node 8+ (Required for Development environment, not required for production)

## Installation

Use of Docker or Homestead is preferred. Else use XAMP/WAMP or the standalone php webserver

Ensure that you follow [Laravel's documentation](https://laravel.com/docs/5.6 ) when you setup your own local environment.

High level steps below -

* Clone this repo and cd into it 

`git clone git@gitlab.com:prakash.karande/myFavouriteAppliances.wow.git`

* Install laravel packages - composer install
* make sure db is running and credentials are setup in the .env file.
* If you have no .env file you can use the example one. Just copy .env.example to .env. Enter your db credentials here.
* Install npm dependencies by running this command - `npm install` (Only required for development environment)

If you want to run app in Dev mode then run following command.

` npm run dev`

If you want to run app in Prod mode then run following command.

`npm run prod`

* Generate laravel app key - `php artisan key:generate`
* Setup the initial database with seed data - `php artisan migrate:refresh --seed`
* Setup Passport encryption keys - `php artisan passport:install`
* Ensure that this variable points to the correct master site in .env file - `DATA_SYNC_URL` (default value is already provided)
* Sync products list from the master site (This will take a while to complete) - `php artisan sync:appliancesData`
* To sync the records from Master site you have to add either of the below commands to a cron job 
 
`* * * * * php /path-to-the-project/artisan schedule:run >> /dev/null 2>&1 `

OR 

`0 0 * * * php /path-to-the-project/artisan sync:appliancesData >> /dev/null 2>&1
`

OR, to run the command once from the command line, run the below command from the project home folder

`php artisan sync:appliancesData`

* Check the app in your browser (either php artisan serve or the virtual host if it has been configured for this project)

## Features in scope

* Synching data from the parent site ()by running the `php artisan sync:appliancesData` command) 
* If a product is deleted from the parent site, then it will also get deleted from our MyFavouriteAppliances.wow app the next time the above sync command is executed.
* Once data gets synched, all data for the MyFavouriteAppliances.wow app is fetched from our database. Only image urls are referred from the parent site.

* Wishlist related features - 

a. Only 1 wishlist per user.

b. If user is a guest/not logged in, then on the main product listing page he will see the 'Login' button, which will take him to the login page.

c. If the user logs in, then on the main product listing page he will be able to add a product to his wishlist.

d. On a shared wishlist url, if the user visits someone else's wishlist (or if he is not logged in), then he gets shown the 'Like' or 'Dislike' buttons.

e. On a shared wishlist url, if the user visits his own wishlist, then he can remove the product from his wishlist (can't like or dislike his own wishlist)

* 'My wishlist' page features -

a. This page will be visible when the user logs in.
 
b. On this page, a user will be able to copy to clipboard his unique wishlist url and share the url with his network.

c. On this page, the user will be able to view all products in his wishlist in a datatable, along with the like and dislike count. He can sort by any column and also delete any item from his wishlist.


## Features out of scope / Future Enhancements

1. Data synching - 

* Currently only products are synched from the master site. In the future even the category hierarchy can be synched.
* Currently the product image urls from the parent site are stored in our MyFavouriteAppliances.wow app. In the future, the actual images can also get downloaded from the urls and stored separately for the MyFavouriteAppliances.wow app (Either local or S3 storage)

2. Wishlist -

* Sharing - Currently only copy to clipboard option is provided. In the future, we can also allow social sharing buttons and sharing by sending an email.
* Like - Dislikes - Currently 1 user can like/dislike a wishlist item multiple times. We currently do not track each like or dislike. In the future, we could track these likes and dislikes for each product (by userid if user is logged in, or by ip if the user is not logged in), and then disallow multiple voting.
* Wishlist sharing slug - Currently a random 16 digits slug gets generated without any duplicates check. In the future, we can add proper slug feature which will re-generate a slug if it is already present.

## External Packages Used

#### Laravel

* [Passport](https://laravel.com/docs/5.6/passport) - For easy API auth. Overkill for this app, but still went ahead and used it.
* [weidner/goutte](https://github.com/dweidner/laravel-goutte) - Laravel based Scraper to sync products with the master site

#### Vue.js
* [Laravel Mix](https://laravel.com/docs/5.6/mix):  For webpack
* [Vuetable - 2](https://github.com/ratiw/vuetable-2): For Wishlist data table
* [Vue-Notification](https://github.com/euvl/vue-notification) : For user notifications after wishlist crud actions
* [Vue Carousel](https://ssense.github.io/vue-carousel/): For showing product images