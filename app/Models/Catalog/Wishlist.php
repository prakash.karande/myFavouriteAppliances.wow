<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Wishlist extends Model
{
    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'wishlists';
    protected $primaryKey = 'id';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'product_id',
        'user_id',
        'likes',
        'dislikes',
    ];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }
}