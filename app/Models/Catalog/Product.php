<?php

namespace App\Models\Catalog;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_id',
        'slug',
        'title',
        'title_image',
        'display_image',
        'gallery_images',
        'rating',
        'price_previous',
        'price_discount',
        'product_info_more',
        'product_basic_description',
        'product_detail',
    ];
    protected $casts = [
        'product_info_more' => 'array',
        'product_basic_description' => 'array',
        'product_detail' => 'array',
        'gallery_images' => 'array'
    ];

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function wishlist()
    {
        //return $this->belongsToMany(Wishlist::class,'product_id');
        return $this->belongsToMany(User::class, 'wishlists');
    }
}