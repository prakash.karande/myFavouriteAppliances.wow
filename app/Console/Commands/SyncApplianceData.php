<?php

namespace App\Console\Commands;

use App\Models\Catalog\Category;
use App\Models\Catalog\Product;
use Illuminate\Console\Command;
use Weidner\Goutte\GoutteFacade;
use Log;

class SyncApplianceData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:appliancesData';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get all Appliance Data from live site';

    /**
     * The array of Url slugs to grab the data.
     *
     * @var string
     */
    protected $categories = [
        'dishwashers',
        'small-appliances'
    ];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach ($this->categories as $category) {
            $message = "Synching products for category: " . $category;
            $this->info($message);
            Log::info($message);
            $this->scrape($category);
            $message = "Completed synching products for category: " . $category;
            $this->info($message);
            Log::info($message);
        }
    }

    /**
     * For scraping data for the specified collection.
     *
     * @param  string $category
     * @return boolean
     */
    public static function scrape($category)
    {
        $crawler = GoutteFacade::request('GET', env('DATA_SYNC_URL').'/'.$category);

        $pages = ($crawler->filter('.result-list-pagination a')->count() > 0)
            ? $crawler->filter('.result-list-pagination a:nth-last-child(1)')->attr('href')
            : 0
        ;
        if(is_string($pages)){
            $parts = parse_url($pages);
            parse_str($parts['query'], $query);
            $pages = $query['page'];
        }
        $categoryDetails = Category::where('slug',$category)->first();
        $allProducts = Product::whereCategoryId($categoryDetails->id)->get();
        $productIds= $allProducts->pluck('id');
        $newMasterSiteIds=collect();

        for ($i = 0; $i < $pages + 1; $i++) {
            if ($i != 0) {
                $crawler = GoutteFacade::request('GET', env('DATA_SYNC_URL').'/'.$category.'?sort=price_asc&page='.$i);
                $crawler->filter('.search-results-product')->each(function ($node) use ($newMasterSiteIds, $allProducts, $categoryDetails) {
                    $title = $node->filter('.product-description h4 a')->count()>0?trim($node->filter('.product-description h4 a')->text()):'';
                    $masterSiteSlug   =  str_slug($title, "-");
                    $displayImage = $node->filter('.product-image img')->count()>0? $node->filter('.product-image img')->attr('src'):'';
                    $titleImage = $node->filter('.article-brand')->count() > 0?$node->filter('.article-brand')->attr('src'):'';

                    $rating = $node->filter('.result-list-rating-text')->count()>0?trim($node->filter('.result-list-rating-text')->text()):'';
                    $productBasicDescription = $node->filter('.result-list-item-desc-list li')->each(function ($node)  {
                        return $node->text();
                    });
                    $pricePrevious = $node->filter('.price-previous')->count() > 0? (float)filter_var( trim($node->filter('.price-previous')->text()), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION ):0;
                    $priceDiscount = $node->filter('.section-title')->count() > 0?(float)filter_var( trim($node->filter('.section-title')->text()), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION ):0;
                    $productInfoMore = $node->filter('.item-info-more')->each(function ($node)  {
                        return $node->text();
                    });

                    $currentProductUrl = $node->filter('.product-description h4 a')->count() > 0?$node->filter('.product-description h4 a')->attr('href'):'';
                    $singleProductCrawler = GoutteFacade::request('GET', $currentProductUrl);

                    $singleProductDetail = $singleProductCrawler->filter('.hidden-xs .hidden-xs .product-detail .tab-content #product-lg-overview')->each(function ($node) {

                        $nodeContent = $node->html();
                        $nodeContent = str_replace("\n","",$nodeContent);
                        $nodeContent = str_replace("\r","",$nodeContent);
                        $nodeContent = stripslashes($nodeContent);
                        return $nodeContent;
                    });

                    $galleryImages = $singleProductCrawler->filter('.carousel-inner .item')->each(function ($node)  {
                        return $node->filter('img')->attr('src');
                    });

                    $product = $allProducts->where('slug',$masterSiteSlug)->first();
                    if(!empty($product)){
                        $product->category_id= $categoryDetails->id;
                        $product->slug= $masterSiteSlug;
                        $product->title= $title;
                        $product->title_image= $titleImage;
                        $product->display_image= $displayImage;
                        $product->gallery_images= $galleryImages;
                        $product->rating= $rating;
                        $product->price_previous= $pricePrevious;
                        $product->price_discount= $priceDiscount;
                        $product->product_info_more= $productInfoMore;
                        $product->product_basic_description= $productBasicDescription;
                        $product->product_detail= $singleProductDetail;
                        $product->save();
                        $newMasterSiteIds->push($product->id);
                    }else{
                        $product = Product::create(
                            ['category_id'=> $categoryDetails->id,
                                'slug'=> $masterSiteSlug,
                                'title'=> $title,
                                'title_image'=> $titleImage,
                                'display_image'=> $displayImage,
                                'gallery_images'=> $galleryImages,
                                'rating'=> $rating,
                                'price_previous'=> $pricePrevious,
                                'price_discount'=> $priceDiscount,
                                'product_info_more'=> $productInfoMore,
                                'product_basic_description'=> $productBasicDescription,
                                'product_detail'=> $singleProductDetail
                            ]);
                        $newMasterSiteIds->push($product->id);
                    }
                });
            }
        }
        $newProducts =$newMasterSiteIds->unique();
        $productsToDelete = $productIds->diff($newProducts);
        /*DELETE THE OLD PRODUCTS FROM TABLE*/
        if($productsToDelete && $productsToDelete->count() > 0){
            foreach ($productsToDelete as $product){
                Product::destroy($product);
            }
        }
        return true;
    }
}
