<?php

namespace App\Lib\Helpers\Data\Query;

class ListQuery {

    protected $query;
    protected $resourceCollection;
    protected $searchableColumnsList;
    protected $includeRelatedFields;

    protected $appendCorsHeader;
    protected $appendCorsOrigin;
    protected $appendCorsMethod;

    /**
     * ListQuery constructor.
     * @param $query
     * @param $searchableColumnsList
     * @param $includeRelatedFields
     */
    public function __construct($query, $resourceCollection, $searchableColumnsList, $includeRelatedFields = false)
    {
        $this->query = $query;
        $this->resourceCollection = $resourceCollection;
        $this->searchableColumnsList = $searchableColumnsList;
        $this->includeRelatedFields = $includeRelatedFields;

        $this->appendCorsHeader = false;
        $this->appendCorsOrigin = "*";
        $this->appendCorsMethod = "GET";
    }


    protected function hasFilter(){
        return false;
    }

    public function appendCors($appendCorsOrigin, $appendCorsMethod){
        $this->appendCorsHeader = true;
        $this->appendCorsOrigin = $appendCorsOrigin;
        $this->appendCorsMethod = $appendCorsMethod;
    }

    public function listQuery(){
        $request = request();

        $this->handleSort($request);

        $this->handleSearchFilter($request);

        $perPage = $request->exists('per_page') ? (int) $request->per_page : null;

        $pagination = $this->query->paginate($perPage);


        if($this->hasFilter()){
            $response = new $this->resourceCollection($pagination);
            $response = $response->additional(
                ['meta' => [
                    'filter' => $request->filter,
                    ]
                ]
            );
        }else{
            $response = new $this->resourceCollection($pagination);
        }

        if($this->appendCorsHeader){
            return $response->response()
                ->header('Access-Control-Allow-Origin', $this->appendCorsOrigin)
                ->header('Access-Control-Allow-Methods', $this->appendCorsMethod);
        }else{
            return $response;
        }
    }

    /**
     * @param $request
     */
    protected function handleSort($request): void
    {
        if ($request->exists('sort')) {
            // handle multisort
            $sorts = explode(',', $request->sort);
            foreach ($sorts as $sort) {
                if (isset($sort) && strlen($sort) > 0) {
                    list($sortCol, $sortDir) = array_pad(explode('|', $sort, 2), 2, "asc");
                    if ($this->includeRelatedFields || strpos($sortCol, ".") === false) {
                        $this->query = $this->query->orderBy($sortCol, $sortDir);
                    }
                }
            }
        } else {
            $this->query = $this->query->orderBy('id', 'asc');
        }
    }

    /**
     * @param $request
     */
    protected function handleSearchFilter($request): void
    {
        if ($request->exists('filter')) {
            if (!isset($this->searchableColumnsList) && isset($this->query->getModel()->fillable)) {
                $this->searchableColumnsList = collect($this->query->getModel()->fillable);
            }

            $searchableColumnsList = $this->searchableColumnsList;
            if (isset($searchableColumnsList)) {
                $this->query->where(function ($q) use ($request, $searchableColumnsList) {
                    $value = "%{$request->filter}%";

                    foreach ($searchableColumnsList as $searchColumn) {
                        if (isset($searchColumn)) {
                            $q->orWhere($searchColumn, 'like', $value);
                        }
                    }
                });
            }
        }
    }
}