<?php

namespace App\Http\Resources\Submissions;

use App\Http\Resources\Wishlist\WishlistResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class WishlistCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => WishlistResource::collection($this->collection),
        ];
    }
}
