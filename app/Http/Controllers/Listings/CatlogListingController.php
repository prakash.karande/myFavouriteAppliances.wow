<?php

namespace App\Http\Controllers\Listings;

use App\Http\Controllers\Controller;
use App\Models\Catalog\Product;
use App\Models\Catalog\Wishlist;
use Illuminate\Http\Request;

class CatlogListingController extends Controller
{

    public function getProductData()
    {
        $request = request();
        $productsQuery = new Product();

        if($request->has('categoryId')){
            $productsQuery = $productsQuery->where('category_id',$request->get('categoryId'));
        }

        if($request->has('orderBy') && $request->get('orderBy') == 'price_asc'){
            $productsQuery = $productsQuery->orderBy('price_discount','asc');
        }
        if($request->has('orderBy') && $request->get('orderBy') == 'price_desc'){
            $productsQuery = $productsQuery->orderBy('price_discount','desc');
        }
        if($request->has('orderBy') && $request->get('orderBy') == 'title_asc'){
            $productsQuery = $productsQuery->orderBy('title','asc');
        }
        if($request->has('orderBy') && $request->get('orderBy') == 'title_desc'){
            $productsQuery = $productsQuery->orderBy('title','desc');
        }

        $productsQuery = $productsQuery->paginate(20);
        return $productsQuery;
    }



}
