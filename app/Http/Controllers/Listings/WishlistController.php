<?php

namespace App\Http\Controllers\Listings;

use App\Http\Controllers\AppController;
use App\Http\Resources\Submissions\WishlistCollection;
use App\Http\Resources\Wishlist\WishlistResource;
use App\Lib\Helpers\Data\Query\ListQuery;
use App\Models\Catalog\Category;
use App\Models\Catalog\Product;
use App\Models\Catalog\Wishlist;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WishlistController extends AppController
{
    public function index(){
        $user =Auth::user();
        $wishlistQuery = Wishlist::where("user_id",$user->id);
        $listQuery = new ListQuery($wishlistQuery, WishlistCollection::class,collect());
        return $listQuery->listQuery();
    }

    public function showWishlist(){
        $user =Auth::user();
        $wishlist = Wishlist::with("product")->where("user_id",$user->id)->get();
        if(empty($wishlist)){
            abort(404);
        }
        return view('wishlist.manage-wishlist', compact("user"));
    }

    public function store(){
        $request = request();
        $this->validate($request, $this->getValidationRules($request));
        if($request->has('product_id')){
            $inputs = $request->all();
            $wishlist = new Wishlist();
            $wishlist->fill($inputs);
            $wishlist->save();
            return $wishlist;
        }
    }

    public function removeWishlistProduct(Request $request){
        if ($request->ajax()){
            $user = Auth::user();
            $wishlistProduct = Wishlist::where('product_id',$request->get('product_id'))->where('user_id',$request->get('user_id'))->first();

            if(!$wishlistProduct || !$user){
                abort(404);
            }
            if(!$user->id == $wishlistProduct->user_id){
                abort(403);
            }
            $removeProductFormWishlist = $wishlistProduct->delete();
            return strval($removeProductFormWishlist );
        }
    }

    public function destroy($wishListId){
        Wishlist::destroy($wishListId);
    }

    public function getWishlistProducts(){
        $user = Auth::user();
        $wishlist = Wishlist::select('product_id')->where('user_id',$user->id)->get();
        return $wishlist->pluck('product_id');
    }

    public function getValidationRules($request=null) {
        $rules = [
            'user_id' => 'required',
            'product_id' => 'required|unique:wishlists,product_id,NULL,user_id,user_id,'.$request->get('user_id')
        ];
        return $rules;
    }

    public function showWishListToOthers($wishlistSlug){
        $user = User::whereWishlistSlug($wishlistSlug)->get()->first();
        $loggedInUser = Auth::user();
        if(!$user){
            abort(404);
        }
        $categories = Category::all();
        return view("wishlist.show-wishlist", compact("user", "loggedInUser","categories"));
    }

    public function getWishlistProductData($wishlistUserId){
        $request = request();
        $wishlist = User::find($wishlistUserId);
        $wishlistProductsQuery = $wishlist->products();
        if($request->has('orderBy') && $request->get('orderBy') == 'price_asc'){
            $wishlistProductsQuery = $wishlistProductsQuery->orderBy('price_discount','asc');
        }
        if($request->has('orderBy') && $request->get('orderBy') == 'price_desc'){
            $wishlistProductsQuery = $wishlistProductsQuery->orderBy('price_discount','desc');
        }
        $wishlistProductsQuery = $wishlistProductsQuery->paginate(20);
        return $wishlistProductsQuery;
    }

    public function likeWishlistProduct(Request $request){
        $wishlistProduct = Wishlist::where('product_id',$request->get('product_id'))->where('user_id',$request->get('user_id'))->first();
        if(!$wishlistProduct){
            abort(404);
        }
        $wishlistProduct->likes = ++$wishlistProduct->likes;
        $wishlistProduct->save();
        return strval($wishlistProduct->likes );
    }

    public function dislikeWishlistProduct(Request $request){
        $wishlistProduct = Wishlist::where('product_id',$request->get('product_id'))->where('user_id',$request->get('user_id'))->first();

        if(!$wishlistProduct){
            abort(404);
        }
        $wishlistProduct->dislikes = ++$wishlistProduct->dislikes;
        $wishlistProduct->save();
        return strval($wishlistProduct->dislikes );
    }

}
