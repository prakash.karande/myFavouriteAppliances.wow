<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get("/get-product-data", "Listings\CatlogListingController@getProductData");
Route::get("/get-wishlist-product-data/{wishlistUserId}", "Listings\WishlistController@getWishlistProductData");

Route::post("hit-likes", "Listings\WishlistController@likeWishlistProduct");
Route::post("hit-dislikes", "Listings\WishlistController@dislikeWishlistProduct");


Route::group([
        'middleware' => ['auth:api'],
    ], function() {
    Route::apiResource("/mywishlist", "Listings\WishlistController");
    Route::get("/get-wishlist-data", "Listings\WishlistController@getWishlistProducts");
    Route::post("/remove-wishlist", "Listings\WishlistController@removeWishlistProduct");
   /* Route::group(
        [
            'prefix' => 'manage-wishlist',
            'namespace' => 'Listings'
        ], function() {
        Route::get("/index", "WishlistController@index");
    });*/

});