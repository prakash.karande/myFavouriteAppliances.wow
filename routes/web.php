<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;

Route::get('/', function () {
    return view('listings');
});
Route::get('/category/{category_id}', function () {
    return view('listings');
});

Route::get('/category/{category_id}', function () {
    return view('listings');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/wishlist/{wishlistSlug}', 'Listings\WishlistController@showWishListToOthers')->name('wishlist.showToOthers');


Route::group(
    [
        'namespace' => 'Listings',
        'middleware' => ['web', 'auth']
    ], function() {

    Route::get('manage-wishlist', 'WishlistController@showWishlist');

});

Route::get('/logout', function () {
     Auth::logout();
    return redirect('/');
});