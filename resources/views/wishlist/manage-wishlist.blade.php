@extends("layouts.app")
@section("content")
    @php
    $wishlistUrl =url('/wishlist/'.$user->wishlist_slug);
    @endphp

    <div id="vue">
        <notifications group="notify" position="top right"></notifications>
        <div class="row mt-3">
            <div class="col-md-12 my-2">My Wishlist Url: <a href="{{$wishlistUrl}}" >{{$wishlistUrl}}</a></div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Get Link to Share
                    </div>
                    <div class="card-body">
                        <share-url
                                share-url="{{$wishlistUrl}}" >
                        </share-url>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <wishlist-datatable
                  api-url="{{route('mywishlist.index')}}"
            ></wishlist-datatable>
        </div>
    </div>
@endsection

@section("before_scripts")
    <script type="text/javascript">
        var fields = [
            {
                name: '__sequence',
                title: '#',
                titleClass: 'text-center sm:hidden',
                dataClass: 'text-center  sm:hidden'
            },
            {
                name: 'name',
                title: 'Product Name',
                plainTitle: 'Product Name',
                titleClass: 'text-center',
                dataClass: 'text-center',
                sortField: 'name',
            },
            {
                name: '__slot:image',
                title: 'Images',
                titleClass: 'text-center sm:hidden',
                dataClass: 'text-center  sm:hidden'
            },
            {
                name: 'likes',
                title: 'Likes',
                plainTitle:'Likes',
                titleClass: 'text-center sm:hidden',
                dataClass: 'text-center  sm:hidden'
            },
            {
                name: 'dislikes',
                title: 'Dislikes',
                plainTitle: 'Likes',
                titleClass: 'text-center sm:hidden',
                dataClass: 'text-center  sm:hidden'
            },

            {
                name: '__slot:actions',
                title: '{{trans("app.actions.label")}}',
                titleClass: 'text-center',
                dataClass: 'text-center',
                visible: this.hideAllActions
            }
        ];

        var sortOrder = [
            {
                field: 'updated_at',
                sortField: 'updated_at',
                direction: 'desc'
            }
        ];
       /*   function copyToClickboard(wishlistUrl) {
                    let textArea = document.createElement("textarea");
                    let success = false;
                    let cssText =
                        'position:fixed;pointer-events:none;z-index:-9999;opacity:0;';
                    textArea.value = wishlistUrl;
                    textArea.style.cssText = cssText;

                    document.body.appendChild(textArea)

                    textArea.select();
                    document.execCommand('copy')

                }*/

    </script>
@endsection
