<!DOCTYPE html>
<html lang="en" >
    <head>
        <meta charset="UTF-8">
        <title>My Favourite Appliances Wishlist</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'>
        <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    </head>

    <body>
        <div class="main-wrapper">
            <div class="header ">
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <a class="navbar-brand" href="/">My Favourite Appliances</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            @foreach($categories as $category)
                                <li class="nav-item" >
                                    <a class="nav-link" href="/category/{{$category->id}}">{{$category->name}}</a>
                                </li>
                            @endforeach
                        </ul>
                        @if(isset($loggedInUser->id))
                            <a class="logout" href="/logout">Logout</a>
                        @endif
                    </div>
                </nav>
            </div>
            <div class="main" id="vue">
                <notifications group="notify" position="top right"></notifications>
                @if(empty($loggedInUser))
                    <catalog-listing
                            :wishlist-user-id={{$user->id}}
                            :is-wish-list-catalog=true
                    ></catalog-listing>
                @else
                    <catalog-listing
                            :user-id={{$loggedInUser->id}}
                            :wishlist-user-id={{$user->id}}
                            :is-wish-list-catalog=true
                    ></catalog-listing>
                @endif
            </div>
        </div>

        <script src="{{ mix('js/app.js') }}"></script>
    </body>

</html>