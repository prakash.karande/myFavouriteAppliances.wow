
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import Vue from 'vue';
window.Vue = Vue;

require('./bootstrap');


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('products', require('./components/Products.vue'));
Vue.component('wishlist', require('./components/Wishlist.vue'));
Vue.component('catalog-listing', require('./components/CatalogListing.vue'));
Vue.component('share-url', require('./components/ShareUrl.vue'));

Vue.component("wishlist-datatable", require('./components/wishlist/WishlistDatatable.vue'));
Vue.component("vue-datatable", require('./components/VueDatatable.vue'));

import Notifications from 'vue-notification';

Vue.component("notifications", Notifications);

Vue.use(Notifications);

const app = new Vue({
    el: '#vue'
});
