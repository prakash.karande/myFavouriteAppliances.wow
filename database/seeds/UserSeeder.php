<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->delete();

        User::create(['name'=>'Test User1','email' => 'test@test.com', 'password'=> bcrypt('password'),'wishlist_slug'=>str_random()]);
        User::create(['name'=>'Test User2','email' => 'test1@test.com', 'password'=> bcrypt('password'),'wishlist_slug'=>str_random()]);
    }

}