<?php

use App\Models\Catalog\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create(['id' => 1, 'name' => 'Dishwashers','slug'=>'dishwashers']);
        Category::create(['id' => 2, 'name' => 'Small Appliances','slug'=>'small-appliances']);

    }
}