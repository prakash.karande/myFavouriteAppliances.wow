<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->unsigned();
            $table->string('slug', 255)->unique();
            $table->string('title', 255);
            $table->text('title_image')->nullable();
            $table->text('display_image')->nullable();
            $table->string('rating',25)->nullable();
            $table->float('price_previous')->nullable();
            $table->float('price_discount')->nullable();
            $table->json('gallery_images')->nullable();
            $table->json('product_info_more')->nullable();
            $table->json('product_basic_description')->nullable();
            $table->text('product_detail')->nullable();


            $table->timestamps();
            $table->softDeletes();

            $table->foreign('category_id')->references('id')->on('categories')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropForeign(['category_id']);
        });

        Schema::drop('products');
    }
}